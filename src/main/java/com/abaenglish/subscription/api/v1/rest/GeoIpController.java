package com.abaenglish.subscription.api.v1.rest;

import com.abaenglish.subscription.service.IVideoService;
import com.abaenglish.subscription.support.GeoIpHelper;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

@Api(value = "video", description = "Operations about Videos")
@RestController
@RequestMapping(value = "api/v1/geoip", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class GeoIpController {

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Get IP country")
    public String getIPCountryOrigin(HttpServletRequest request)
        throws IOException, GeoIp2Exception {

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

        return this.getIPCountry(ipAddress,request);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{ip:.*}")
    @ApiOperation(value = "Get IP country")
    public String getIPCountry(@PathVariable("ip") String ipAddress, HttpServletRequest request)
        throws IOException, GeoIp2Exception {

        GeoIpHelper geoHelper = new GeoIpHelper();
        Country country = geoHelper.getCountry(ipAddress);

        String countryIsoCode2 = country.getIsoCode();

        JSONObject json = new JSONObject();
        try {
            json.put("IP", ipAddress);
            json.put("CountryIso", countryIsoCode2);
        }
        catch(JSONException jioe) {
            return "{error1:" + jioe.getMessage() + "}";
        }

        return json.toString();
    }

}
