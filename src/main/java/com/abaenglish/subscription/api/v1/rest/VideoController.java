package com.abaenglish.subscription.api.v1.rest;

import com.abaenglish.subscription.service.IVideoService;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Api(value = "video", description = "Operations about Videos")
@RestController
@RequestMapping(value = "api/v1/video", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class VideoController {

    @Autowired
    private IVideoService videoService;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Get Video")
    public void getSubscriptionbyAccount(@RequestParam("url") String url, HttpServletRequest request, HttpServletResponse response)
        throws IOException, GeoIp2Exception {

        try {
            url = videoService.getVideoUrl(url, request);
        } catch (Exception e) {
        }

        response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
        response.setHeader("Location", url);
    }

}
