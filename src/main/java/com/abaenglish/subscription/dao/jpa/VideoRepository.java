package com.abaenglish.subscription.dao.jpa;


import com.abaenglish.boot.data.repository.BaseRepository;
import com.abaenglish.subscription.domain.Video;

import java.util.Optional;

public interface VideoRepository extends BaseRepository<Video, Long> {
    Optional<Video> findByUrlHD(String urlHD);
    Optional<Video> findByUrlSD(String urlSD);
    Optional<Video> findByMobileSD(String mobileHD);
    Optional<Video> findOneByIdUnitAndIdSectionAndIdProvider(Integer idUnit, Integer idSection, Integer idProvider);
}

