package com.abaenglish.subscription.domain;

import com.abaenglish.boot.domain.DomainObject;

import javax.persistence.*;

@Entity
@Table(name = "video")
public class Video extends DomainObject {

//    private static final long serialVersionUID = 8397837743463390960L;

    public static final String COUNTRY_CODE_ISO2_CH = "CN";

    public static final Integer VIDEO_TYPE_DEFAULT = 0;
    public static final Integer VIDEO_TYPE_HD = 1;
    public static final Integer VIDEO_TYPE_SD = 2;
    public static final Integer VIDEO_TYPE_MOBILEHD = 3;

    public static final Integer VIDEO_PROVIDER_ID_VIMEO = 0;
    public static final Integer VIDEO_PROVIDER_ID_VZAAR = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "idUnit", insertable = true, updatable = true, nullable = false)
    private Integer idUnit;

    @Column(name = "idSection", insertable = true, updatable = true, nullable = false)
    private Integer idSection;

    @Column(name = "idProvider", insertable = true, updatable = true, nullable = false)
    private Integer idProvider;

    @Column(name = "urlHD", insertable = true, updatable = true, nullable = false, length = 255)
    private String urlHD;

    @Column(name = "urlSD", insertable = true, updatable = true, nullable = false, length = 255)
    private String urlSD;

    @Column(name = "mobileSD", insertable = true, updatable = true, nullable = false, length = 255)
    private String mobileSD;

    @Column(name = "preview", insertable = true, updatable = true, nullable = false, length = 255)
    private String preview;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdUnit() {
        return idUnit;
    }

    public void setIdUnit(Integer idUnit) {
        this.idUnit = idUnit;
    }

    public Integer getIdSection() {
        return idSection;
    }

    public void setIdSection(Integer idSection) {
        this.idSection = idSection;
    }

    public Integer getIdProvider() {
        return idProvider;
    }

    public void setIdProvider(Integer idProvider) {
        this.idProvider = idProvider;
    }

    public String getUrlHD() {
        return urlHD;
    }

    public void setUrlHD(String urlHD) {
        this.urlHD = urlHD;
    }

    public String getUrlSD() {
        return urlSD;
    }

    public void setUrlSD(String urlSD) {
        this.urlSD = urlSD;
    }

    public String getMobileSD() {
        return mobileSD;
    }

    public void setMobileSD(String mobileSD) {
        this.mobileSD = mobileSD;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Video video = (Video) o;

        if (!id.equals(video.id)) return false;
        if (!idUnit.equals(video.idUnit)) return false;
        if (!idSection.equals(video.idSection)) return false;
        if (!idProvider.equals(video.idProvider)) return false;
        if (!urlHD.equals(video.urlHD)) return false;
        if (!urlSD.equals(video.urlSD)) return false;
        if (!mobileSD.equals(video.mobileSD)) return false;
        return preview.equals(video.preview);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + idUnit.hashCode();
        result = 31 * result + idSection.hashCode();
        result = 31 * result + idProvider.hashCode();
        result = 31 * result + urlHD.hashCode();
        result = 31 * result + urlSD.hashCode();
        result = 31 * result + mobileSD.hashCode();
        result = 31 * result + preview.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Video{" +
            "id=" + id +
            ", idUnit=" + idUnit +
            ", idSection=" + idSection +
            ", idProvider=" + idProvider +
            ", urlHD='" + urlHD + '\'' +
            ", urlSD='" + urlSD + '\'' +
            ", mobileSD='" + mobileSD + '\'' +
            ", preview='" + preview + '\'' +
            '}';
    }
}
