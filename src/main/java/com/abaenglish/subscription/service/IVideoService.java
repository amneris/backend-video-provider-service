package com.abaenglish.subscription.service;

import com.abaenglish.subscription.domain.Video;
import com.maxmind.geoip2.exception.GeoIp2Exception;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Optional;

public interface IVideoService {
    Video update(Long id, Video item);

    Optional<Video> findByUrlHD(String urlHD);

    Optional<Video> findByUrlSD(String urlSD);

    Optional<Video> findByMobileSD(String mobileSD);

    Optional<Video> findOneByIdUnitAndIdSectionAndIdProvider(Integer idUnit, Integer idSection, Integer idProvider);

    String getVideoUrl(String url, HttpServletRequest request) throws IOException, GeoIp2Exception;
}
