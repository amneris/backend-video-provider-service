package com.abaenglish.subscription.service;

import com.abaenglish.subscription.dao.jpa.VideoRepository;
import com.abaenglish.subscription.domain.Video;
import com.abaenglish.subscription.support.GeoIpHelper;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Optional;

@Service
public class VideoService implements IVideoService {

    public static final String GEOIP2_DB_FILE_NAME = "GeoLite2-City.mmdb";

    @Autowired
    private VideoRepository videoRepository;

    public Video update(Long id, Video item) {
        Video itemUpdated = videoRepository.findOne(id).get();

        itemUpdated.setId(id);

        return (Video) videoRepository.save(itemUpdated);
    }

    public Optional<Video> findByUrlHD(String urlHD) {
        return videoRepository.findByUrlHD(urlHD);
    }

    public Optional<Video> findByUrlSD(String urlSD) {
        return videoRepository.findByUrlSD(urlSD);
    }

    public Optional<Video> findByMobileSD(String mobileSD) {
        return videoRepository.findByMobileSD(mobileSD);
    }

    public Optional<Video> findOneByIdUnitAndIdSectionAndIdProvider(Integer idUnit, Integer idSection, Integer idProvider) {
        return videoRepository.findOneByIdUnitAndIdSectionAndIdProvider(idUnit, idSection, idProvider);
    }

    /**
     * GET IP ADDRESS FROM REQUEST
     *
     * @param request
     * @return
     */
    private String getIpAddress(HttpServletRequest request) {

        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = request.getRemoteAddr();
        }

//        ipAddress = "59.125.171.252"; // TAIWAN
//        ipAddress = "104.222.145.251"; // HONG KONG
//        ipAddress = "182.239.127.137"; // HONG KONG
//        ipAddress = "218.240.147.232"; // CHINA
//        ipAddress = "182.44.12.104"; // CHINA

        return ipAddress;
    }

    /**
     * @param url
     * @param request
     * @return
     * @throws IOException
     * @throws GeoIp2Exception
     */
    public String getVideoUrl(String url, HttpServletRequest request) throws IOException, GeoIp2Exception {

        /**
         * GET IP ADDRESS FROM REQUEST
         */
        String ipAddress = this.getIpAddress(request);


        /**
         * GET COUNTRY BY IP
         */
        GeoIpHelper geoHelper = new GeoIpHelper();
        Country country = geoHelper.getCountry(ipAddress);

        String countryIsoCode2 = country.getIsoCode();


        /**
         * CHANGE VIDEO URL FOR CHINA
         */
        Integer videoType = Video.VIDEO_TYPE_DEFAULT;

        if (countryIsoCode2.compareTo(Video.COUNTRY_CODE_ISO2_CH) == 0) {

            Optional<Video> videoHD = this.findByUrlHD(url);

            Video video = new Video();

            if (videoHD.isPresent()) {

                video = videoHD.get();

                videoType = Video.VIDEO_TYPE_HD;
            } else {

                Optional<Video> videoSD = this.findByUrlSD(url);

                if (videoSD.isPresent()) {

                    video = videoSD.get();

                    videoType = Video.VIDEO_TYPE_SD;
                } else {

                    Optional<Video> videoMobileSd = this.findByMobileSD(url);

                    if (videoMobileSd.isPresent()) {

                        video = videoMobileSd.get();

                        videoType = Video.VIDEO_TYPE_MOBILEHD;
                    }
                }
            }

            if (videoType.compareTo(Video.VIDEO_TYPE_DEFAULT) != 0) {

                Optional<Video> optionalVideo = this.findOneByIdUnitAndIdSectionAndIdProvider(video.getIdUnit(), video.getIdSection(), Video.VIDEO_PROVIDER_ID_VZAAR);

                if (optionalVideo.isPresent()) {

                    Video finalVideo = optionalVideo.get();

                    if (videoType.compareTo(Video.VIDEO_TYPE_HD) == 0) {
                        url = finalVideo.getUrlHD();
                    } else if (videoType.compareTo(Video.VIDEO_TYPE_SD) == 0) {
                        url = finalVideo.getUrlSD();
                    } else if (videoType.compareTo(Video.VIDEO_TYPE_MOBILEHD) == 0) {
                        url = finalVideo.getMobileSD();
                    }
                }
            }
        }

        return (String) url;

    }

}
