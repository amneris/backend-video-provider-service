package com.abaenglish.subscription.support;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Country;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

/**
 */
public class GeoIpHelper {

    private DatabaseReader reader = null;

    public final String GEOIP2_DB_FILE_NAME = "GeoLite2-City.mmdb";

    public Country getCountry(String ipAddress) throws IOException, GeoIp2Exception {

        if ( reader == null ) {
            ClassLoader classLoader = getClass().getClassLoader();
            File database = new File(classLoader.getResource(this.GEOIP2_DB_FILE_NAME).getFile());

            reader = new DatabaseReader.Builder(database).build();
        }

        InetAddress ipAddressss = InetAddress.getByName(ipAddress);
        CityResponse response2 = reader.city(ipAddressss);
        Country country = response2.getCountry();

        return country;
    }
}
