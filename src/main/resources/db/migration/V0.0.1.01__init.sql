
DROP TABLE IF EXISTS video;

CREATE TABLE video (
  id            BIGSERIAL NOT NULL,
  id_unit        integer not null,
  id_section     integer not null,
  id_provider    integer not null,
  urlHD         VARCHAR(255),
  urlSD         VARCHAR(255),
  mobileSD      VARCHAR(255),
  preview       VARCHAR(255),
  CONSTRAINT pk_video PRIMARY KEY (id)
);
